FROM dkja/devel-base

RUN     cd /tmp \
    && git clone https://github.com/google/googletest \
    && cd googletest \
    && mkdir build \
    && cd build \
    && cmake .. \
    && make install \
    && rm -rf /tmp/googletest